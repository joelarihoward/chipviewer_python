from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QPushButton, QFrame, QGridLayout, QGroupBox, \
QComboBox, QLineEdit
import PyQt6.QtWidgets as QtWidgets
from PyQt6 import QtCore, QtTest
from db import get_masksets, get_lots, get_wafers, get_nanotronics_scan_ids, get_maskset_info, \
    nanotronics_tile_query, save_image
import sys
import numpy as np
from shapely.geometry import Polygon
import shapely
from PIL import Image
import os, shutil

YELLOW = '#ccbe00'
RED = '#a60d02'
GREEN = '#007a31'
WHITE = '#FFFFFF'
BLACK = '#000000'
GRAY = '#888888'
LIGHT_GRAY = '#AAAAAA'
OFF_WHITE = '#CCCCCC'
BLUE = '#0000CC'
LIGHT_GREEN = '#00FF7F'

class RPushButton(QPushButton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color = WHITE
        self.bg = BLUE
        self.setMinimumHeight(40)

    def setEnabled(self, a0: bool) -> None:
        if a0:
            self.set_color(self.color, self.bg)
        else:
            self.setStyleSheet(
                f'RPushButton {{background-color: {OFF_WHITE}; color: {LIGHT_GRAY}; margin: 2px 6px; padding: 8px;}}')
        super().setEnabled(a0)

    def set_color(self, color=WHITE, bg=BLUE):
        self.color = color
        self.bg = bg
        self.setStyleSheet(f'RPushButton {{background-color: {bg}; color: {color}; margin: 2px 6px; padding: 8px;}}')

class Launcher:
    def __init__(self):
        self.app = QApplication([])
        self.main_window = WaferSelectionWindow()

    def launch(self):
        self.main_window.show()
        sys.exit(self.app.exec())


class WaferSelectionWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.maskset = ''
        self.lot = ''
        self.wafer = ''
        self.die = ''
        self.scan_id = 0
        self.x_shim = 0
        self.y_shim = 0

        width = 800
        self.setMinimumSize(width, width)
        self.setWindowTitle('Chip Viewer')

        self.layout = QGridLayout(self)
        self.wafer_selection_layout()

        self.next = None
        self.lot_error_dialog = None
        self.wafer_error_dialog = None
        self.scan_id_dialog = None
        self.set_initial_button_states()
        self.setFocusPolicy(QtCore.Qt.FocusPolicy.StrongFocus)
        self.show()

    def wafer_selection_layout(self):
        """ Wafer form """
        self.wafer_group_box = QGroupBox('Select Wafer')
        self.wafer_layout = QGridLayout()

        self.maskset_label = QLabel('Maskset: ')
        self.maskset_list = QComboBox()
        self.lot_label = QLabel('Lot: ')
        self.lot_list = QComboBox()
        self.wafer_label = QLabel('Wafer: ')
        self.wafer_list = QComboBox()
        self.scan_id_label = QLabel('Scan ID: ')
        self.scan_id_list = QComboBox()
        self.die_label = QLabel('Die: ')
        self.die_list = QComboBox()
        self.x_shim_label = QLabel('Left shift (microns): ')
        self.x_shim_entry = QLineEdit()
        self.y_shim_label = QLabel('Down shift (microns): ')
        self.y_shim_entry = QLineEdit()

        self.maskset_info = None

        self.load_die_image_button = RPushButton('Process Die Image')
        self.load_die_image_button.set_color(WHITE, BLUE)
        self.purge_tiles_button = RPushButton('Delete all tile files')
        self.purge_tiles_button.set_color(WHITE, RED)


        self.wafer_layout.addWidget(self.maskset_label, 0, 0)
        self.wafer_layout.addWidget(self.maskset_list, 0, 1)
        self.wafer_layout.addWidget(self.lot_label, 1, 0)
        self.wafer_layout.addWidget(self.lot_list, 1, 1)
        self.wafer_layout.addWidget(self.wafer_label, 2, 0)
        self.wafer_layout.addWidget(self.wafer_list, 2, 1)
        self.wafer_layout.addWidget(self.scan_id_label, 3, 0)
        self.wafer_layout.addWidget(self.scan_id_list, 3, 1)
        self.wafer_layout.addWidget(self.die_label, 4, 0)
        self.wafer_layout.addWidget(self.die_list, 4, 1)

        self.wafer_layout.addWidget(self.x_shim_label, 5, 0)
        self.wafer_layout.addWidget(self.x_shim_entry, 5, 1)
        self.wafer_layout.addWidget(self.y_shim_label, 6, 0)
        self.wafer_layout.addWidget(self.y_shim_entry, 6, 1)
        
        self.wafer_layout.addWidget(self.load_die_image_button, 7, 0, 1, 2)
        self.wafer_layout.addWidget(self.purge_tiles_button, 8, 0, 1, 2)

        self.wafer_group_box.setLayout(self.wafer_layout)
        self.layout.addWidget(self.wafer_group_box, 0, 0)

        """ Callbacks """
        self.maskset_list.currentTextChanged[str].connect(self.select_maskset_callback)
        self.lot_list.currentTextChanged[str].connect(self.select_lot_callback)
        self.wafer_list.currentTextChanged[str].connect(self.select_wafer_callback)
        self.scan_id_list.currentTextChanged[str].connect(self.select_scan_id_callback)
        self.die_list.currentTextChanged[str].connect(self.select_die_callback)
        self.load_die_image_button.clicked.connect(self.load_die_image_callback)
        self.purge_tiles_button.clicked.connect(self.purge_tiles_callback)

        self.x_shim_entry.returnPressed.connect(self.x_shim_entry_callback)
        self.y_shim_entry.returnPressed.connect(self.y_shim_entry_callback)

    def set_initial_button_states(self):
        """ Sets the initial button states for components on this page. """

        self.lot_list.setEnabled(False)
        self.wafer_list.setEnabled(False)
        self.load_die_image_button.setEnabled(False)

        self.maskset_list.addItems(get_masksets()['maskset_name'])


    def select_maskset_callback(self, maskset):
        self.maskset = maskset
        self.lot_list.clear()
        self.wafer_list.clear()
        self.scan_id_list.clear()
        self.die_list.clear()
        lots = get_lots(self.maskset)['lot_name'].values
        if not list(lots):
            self.lot_error_dialog = QtWidgets.QErrorMessage()
            self.lot_error_dialog.showMessage('Oops! There is no data for this maskset!')
            self.lot_error_dialog.show()
        else:
            lots = [" "] + list(lots)
            self.lot_list.addItems(lots)
            self.lot_list.setEnabled(True)

    def select_lot_callback(self, lot):
        if not lot.strip():
            return
        self.lot = lot
        self.wafer_list.clear()
        wafers = get_wafers(self.maskset, self.lot)['wafer_name'].values
        if not list(wafers):
            self.wafer_error_dialog = QtWidgets.QErrorMessage()
            self.wafer_error_dialog.showMessage('Oops! There is no wafer for this maskset and lot!')
            self.wafer_error_dialog.show()
        else:
            wafers = [" "] + list(wafers)
            self.wafer_list.addItems(wafers)
            self.wafer_list.setEnabled(True)

    def select_wafer_callback(self, wafer):
        """ This function sets the wafer value.
        :param wafer: wafer value that was selected
        """
        if not wafer.strip():
            return
        self.wafer=wafer
        self.scan_id_list.clear()
        scan_ids = [str(i) for i in get_nanotronics_scan_ids(self.maskset, self.lot, self.wafer)]
        if not list(scan_ids):
            self.scan_id_dialog = QtWidgets.QErrorMessage()
            self.scan_id_dialog.showMessage('Oops! There are no scan IDs for this wafer!')
            self.scan_id_dialog.show()
        else:
            scan_ids = [" "] + list(scan_ids)
            self.scan_id_list.addItems(scan_ids)
            self.scan_id_list.setEnabled(True)

    def select_scan_id_callback(self, scan_id):
        if not scan_id.strip():
            return
        self.scan_id=scan_id
        self.maskset_info = get_maskset_info(self.maskset)
        self.die_list.clear()
        dies = self.maskset_info.index
        dies = [" "] + list(dies)
        self.die_list.addItems(dies)
        self.die_list.setEnabled(True)
        
        
    def select_die_callback(self, die):
        if not die.strip():
            return
        self.die=die
        self.load_die_image_button.setEnabled(True)

    def x_shim_entry_callback(self):
        value = self.x_shim_entry.text()
        if value!='':
            self.x_shim = float(value)

    def y_shim_entry_callback(self):
        value = self.y_shim_entry.text()
        if value!='':
            self.y_shim = float(value)

    def purge_tiles_callback(self):
        folder = "wafer_images/tile_images"
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                os.unlink(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

    def load_die_image_callback(self):
        tile_info = nanotronics_tile_query(self.scan_id)
        
        center_x = np.mean([min(tile_info.x),max(tile_info.x)+max(tile_info.width)])
        center_y = np.mean([min(tile_info.y),max(tile_info.y)+max(tile_info.height)])

        tile_polygons = [Polygon([(row[1].x,row[1].y), 
                          (row[1].x,row[1].y+row[1].height),
                          (row[1].x+row[1].width, row[1].y+row[1].height),
                          (row[1].x+row[1].width, row[1].y)]) 
                 for row in tile_info.iterrows()]
        tile_info.insert(2, "polygon", tile_polygons)

        die_data = self.maskset_info.loc[self.die]
        die_width = die_data.x_dimension
        die_height = die_data.y_dimension

        x_shim = self.x_shim #Move the microscope to the right
        y_shim = self.y_shim #Move it down.

        die_x = center_x + die_data.maskset_seat_x - die_width/2 + x_shim
        die_y = center_y - die_data.maskset_seat_y - die_height/2 + y_shim

        die_polygon = Polygon([(die_x, die_y), 
                       (die_x+die_width, die_y),
                       (die_x+die_width, die_y+die_height),
                       (die_x, die_y+die_height)])
        
        die_tile_data = tile_info.loc[tile_info.index[tile_info.apply(lambda row: row.polygon.intersects(die_polygon), axis=1)]]
        die_tile_filenames = die_tile_data.filename.unique()
        
        for filename in die_tile_filenames:
            if not os.path.isfile(f"wafer_images/tile_images/{filename}"):
                save_image(scan_id=self.scan_id, filename=filename, prefix="full")

        # Stitch Images
        image1 = Image.open(f"wafer_images/tile_images/{die_tile_data.iloc[0].filename}")
        tile_width = die_tile_data.iloc[0].width
        tile_height = die_tile_data.iloc[0].height
        width_conversion = image1.size[0]/tile_width
        height_conversion = image1.size[1]/tile_height

        scaled_polygons = []
        images = []
        for row in die_tile_data.iterrows():
            image = Image.open(f"wafer_images/tile_images/{row[1].filename}")
            images.append(image)
            scaled_polygon = shapely.affinity.scale(row[1].polygon, xfact=width_conversion, yfact=height_conversion, origin=(0,0))
            scaled_polygons.append(scaled_polygon)

        min_x_vals = list()
        min_y_vals = list()
        for i, polygon in enumerate(scaled_polygons):
            coords = list(polygon.exterior.coords)
            x_coords = [i[0] for i in coords]
            y_coords = [i[1] for i in coords]
            min_x_vals.append(min(x_coords))
            min_y_vals.append(min(y_coords))

        if die_tile_data.iloc[0].width<2000:
            overlap = 0
        else:
            overlap = 0.06
        overlapped_polygons = scaled_polygons.copy()
        for i, min_x_val in enumerate(min_x_vals):
            shift_num = np.sum(min_x_val>np.unique(min_x_vals))
            overlapped_polygons[i] = shapely.affinity.translate(overlapped_polygons[i], xoff=-overlap*tile_width*shift_num, yoff=0)
        for i, min_y_val in enumerate(min_y_vals):
            shift_num = np.sum(min_y_val>np.unique(min_y_vals))
            overlapped_polygons[i] = shapely.affinity.translate(overlapped_polygons[i], xoff=0, yoff=-overlap*tile_height*shift_num)

        all_coords = [j for i in overlapped_polygons for j in list(i.exterior.coords)]
        x_coords = [i[0] for i in all_coords]
        y_coords = [i[1] for i in all_coords]

        min_x = min(x_coords)
        max_x = max(x_coords)
        min_y = min(y_coords)
        max_y = max(y_coords)
        translated_polygons = [shapely.affinity.translate(i, xoff=-min_x, yoff=-min_y, zoff=0.0) for i in overlapped_polygons]
        new_image = Image.new('RGB',(int(max_x - min_x), int(max_y-min_y)), (250,250,250))

        for i in range(len(translated_polygons)):
            new_image.paste(images[i], [int(j) for j in list(translated_polygons[i].exterior.coords)[0]])

        new_image.save(f"wafer_images/{self.die}.jpg","JPEG")

        self.load_die_image_button.setText("Finished!")
        self.load_die_image_button.set_color(WHITE, GREEN)
        QtTest.QTest.qWait(5000)

        self.load_die_image_button.setText("Process Die Image")
        self.load_die_image_button.set_color(WHITE, BLUE)



if __name__ == "__main__":
    launcher = Launcher()
    launcher.launch()