# chipviewer_python



## Installation

Navigate into the `chipviewer` folder and run `poetry install`. This will create the necessary poetry environment. 

## Usage

First, make sure you are connected to the VPN. In the `chipviewer` folder run `poetry shell` to activate the project python environment, then run `python3 app.py` to launch the app.

It will prompt for the wafer, scan, and die info. Then press `Process Die Image`. It will download the `Tile_xxx.png` files for that die and stitch them together, producing a `die_name.png` file in the `wafer_images` folder. However it may not perfectly center on the die, so there are parameters `Left shift` and `Down shift` which are corrective offsets. For example if the user set `Left shift=2000` and reran the image processing, the new stitched image will appear as if the microscope were 2000 microns to the left (and likewise for `Down shift`). Notably, once the correct offset is found for one die, that same offset should then apply for all dies on that wafer. 

Finally, there's a button to delete all Tile files to free up local storage space.

Please direct any questions to jhoward@rigetti.com.