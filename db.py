from psycopg2 import connect
import warnings
from pandas import read_sql_query
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import hashes
import datetime
import urllib
from botocore.signers import CloudFrontSigner




conn = connect(dbname='production', user="measurements_rw", password="cosmic_starfruit", host="postgres01.lab.rigetti.com")

def get_query(query):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        df = read_sql_query(query,con=conn)
    return df

def get_masksets():
    query = "SELECT ms.maskset_name FROM metrology.maskset ms;"
    df = get_query(query).sort_values(by='maskset_name', ascending=True)
    return df


def get_lots(maskset):
    query = f"""
        SELECT l.lot_name
        FROM metrology.lot l
        JOIN metrology.maskset m ON l.maskset_id = m.maskset_id
        WHERE m.maskset_name = '{maskset}'; 
        """
    df = get_query(query).sort_values(by='lot_name', ascending=True)
    return df

def get_wafers(maskset, lot):
    query = f"""
        SELECT w.wafer_name
        FROM metrology.maskset m
        JOIN metrology.lot l ON m.maskset_id = l.maskset_id
        JOIN metrology.wafer w ON l.lot_id = w.lot_id
        WHERE m.maskset_name = '{maskset}' AND l.lot_name = '{lot}';
        """
    df = get_query(query).sort_values(by='wafer_name', ascending=True).astype({'wafer_name': str})
    return df

def nanotronics_tile_query(scan_id):
    query = f"select * from metrology.nanotronics_tile where nanotronics_scan_id = {scan_id}"
    return get_query(query)


def get_nanotronics_scan_ids(maskset_name, lot_name, wafer_name):
    query = f"SELECT nanotronics_scan_id FROM metrology.wafer_fancy wf \
            JOIN metrology.nanotronics_scan ns ON wf.wafer_id=ns.wafer_id \
            WHERE wf.maskset_name='{maskset_name}' AND wf.lot_name='{lot_name}' \
            AND wf.wafer_name='{str(wafer_name)}'"
    return get_query(query).nanotronics_scan_id.values

def get_maskset_info(maskset):
    query = f"SELECT die_name, maskset_seat_x, maskset_seat_y, x_dimension, y_dimension \
              FROM metrology.maskset ms \
              JOIN metrology.maskset_seat mss ON ms.maskset_id=mss.maskset_id \
              JOIN metrology.design_fancy df ON mss.design_id=df.design_id \
              WHERE maskset_name=\'{maskset}\'"
    return get_query(query).set_index('die_name')


CLOUDFRONT_PRIVATE_KEY_FILE = "/Users/jhoward/Desktop/chipviewer/cloudfront_private_key.pem"
cloudfront_key_data = open(CLOUDFRONT_PRIVATE_KEY_FILE, 'rb').read()

NANOTRONICS_BASE_URL = "https://rigetti-nanotronics.qsw.rigetti.com/"

def rsa_signer(message):
    with open(CLOUDFRONT_PRIVATE_KEY_FILE, 'rb') as key_file:
        private_key = serialization.load_pem_private_key(
            key_file.read(),
            password=None,
            backend=default_backend()
        )
    return private_key.sign(message, padding.PKCS1v15(), hashes.SHA1())

key_id = "APKAJUJVLZSZXKMM5JPQ"
expire_date = datetime.datetime(2024, 1, 1)
cloudfront_signer = CloudFrontSigner(key_id, rsa_signer)

def signed_url(scan_id, filename, prefix):
    url = NANOTRONICS_BASE_URL + str(scan_id) + "/" + str(prefix) + "/" + filename
    return cloudfront_signer.generate_presigned_url(url, date_less_than=expire_date)

def save_image(scan_id, filename, prefix):
    urllib.request.urlretrieve(signed_url(scan_id=scan_id, filename=filename, prefix=prefix), f"wafer_images/tile_images/{filename}")